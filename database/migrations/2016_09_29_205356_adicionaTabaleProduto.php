<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionaTabaleProduto extends Migration
{
      public function up()
    {
       Schema::create('produtos', function(Blueprint $table){
               $table->increments('id');
               $table->string('nome');
               $table->string('descricao');
               $table->string('foto');
               $table->double('preco');
               $table->string('categoria');
               $table->string('caracteristicas');
         
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produto');
    }
}

