<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   
  public function rules()
    {
    
        return [
            
            'nome'=>'required|max:100',
            'descricao'=>'required|min:10',
            'preco'=>'required',
            'categoria'=>'',
            'caracteristicas'=>''
            

        ];
    }
    
    public function messages(){
    
        return [
            'required'=>'O :attribute é obrigatorio'
        ];
    }
}
