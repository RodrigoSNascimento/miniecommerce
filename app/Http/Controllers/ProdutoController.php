<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produto;
use App\Http\Requests\ProdutoRequest;
use App\Categoria;

class ProdutoController extends Controller
{
    
    
    public function novo(){
        
        return view('formularioProduto')->with('categorias',Categoria::all());;
    
    }
    
    public function adiciona(ProdutoRequest $request){
        
        
        $produto['nome']=  $request->input('nome');
        $produto['descricao']=  $request->input('descricao');
        $produto['preco']=  $request->input('preco');
        
        if($request->file('foto')!= null){
        
        $nomePhoto = str_random(12).".png";
        Storage::put($nomePhoto,file_get_contents($request->file('foto')->getRealPath()));
        $produto['foto']=   $nomePhoto;
        }
   
        Produto::create($produto);
        
        return redirect("/produtos"); //->withInput(); Para usar Old
    
    }
    
    public function lista($busca=null){
        
        if($busca==null){
            $produtos = Produto::all();               
            
        }else{
            
            $produtos = Produto::where("nome", 'like', '%'.$busca.'%')
                ->orWhere('descricao', 'like', '%'.$busca.'%')
                ->get();;
        }
        
        return view('/produtos')->with("produtos",$produtos);
        
    }
      
    public function detalhes($id){
     
        $produto = Produto::find($id);
        return view('produtoDetalhe')->with('p',$produto);
    }
    
    
    public function buscarPor($campo, $valor, $colunas = array('*')) {
        
        $produto = new Produto();
        return $produto->where($campo, '=', $valor)->first($colunas);
    }
    
}



