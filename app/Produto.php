<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
//    protected $table = "produto";
    public $timestamps= false; 
    public $fillable = array("nome","descricao","preco","categoria","caracteristicas","foto");

    
    public function Categoria(){
        
        return $this->belongsToMany('App\Categoria');
    
    }
    
    public function Caracteristica(){
    
        return $this->belongsToMany('App\Categoria');
    
    }
}
