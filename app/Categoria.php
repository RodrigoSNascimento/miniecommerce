<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    public $timestamps= false; 
    public $fillable = array("nome");
    
    
    public function Produto(){

        return $this->belongsToMany('App\Produto');

    }
}
