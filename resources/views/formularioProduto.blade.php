@extends('principal')

@section('conteudo')


<br>
    <div class="container">
        @if(empty(!$errors->all()))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $erro)
                    <li>{{$erro}}</li>
                    @endforeach
                </ul>
            </div>   
        @endif
        <form action="adicionaproduto" method="POST" enctype="multipart/form-data" >

            <div class="form-group"></div>
            <input type="hidden" value="{{csrf_token()}}"  name="_token">

            <div class="form-group">
                <label> Foto
                <input type="file" name="foto">
            </div>
            <div class="form-group">
                <label> Nome:</label>
                <input type="text" class="form-control" name="nome">
            </div>

            <div class="form-group">
                <label> Descrição:</label>
                <textarea class="form-control" name="descricao"></textarea>
            </div>
            <!-- Imagem -->
            <div class="form-group">
                <label> Preço :</label>
               <input type="text" class="form-control" name="preco">
            </div>
            <div class="form-group">
            <select class="selectpicker" multiple name="categoria_id">
                @foreach($categorias as $c)
                <option value="{{$c->id}}">{{$c->nome}} </option>
                @endforeach
            </select>
            </div>

                        
            <button type="submit" class="btn btn-primary">Cadastrar</button>


        </form>
    </div>
@stop