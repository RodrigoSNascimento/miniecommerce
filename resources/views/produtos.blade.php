@extends('principal')

@section('conteudo')

<h1>Lista De Produtos</h1>
       
        @if(old('nome'))
            <div class="alert alert-success">
              <strong>Sucesso!</strong> 
                O Curso {{ old('nome') }} foi adicionado.
            </div>
        <br>
        @endif

        <table class="table">
            
            @foreach ($produtos as $p)
       
            <tr>
                <td><img src="..\storage\app\{{$p->foto}}"</td>
                <td>{{$p->nome}}</td>
                <td>{{$p->descricao}}</td>
                <td>{{$p->preco}}</td>
                <td>
                    <a href="/produto/detalhes/{{$p->id}}">Vizualizar</a>
                </td>
                <td>
                    <a href="/cursos/remove/{{$p->id}}">Remover</a>
                </td>
            </tr>
        @endforeach
        </table>

@stop